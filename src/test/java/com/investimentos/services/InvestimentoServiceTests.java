package com.investimentos.services;

import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    Investimento investimento;

    Simulacao simulacao;

    @Autowired
    InvestimentoService investimentoService;

    @BeforeEach
    public void iniciar(){
        investimento = new Investimento();
        investimento.setIdInvest(1);
        investimento.setDscInvest("Descricao Investimento");
        investimento.setNomeInvest("Nome Investimento");
        investimento.setPercentInvest(10);
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.ALTO);
    }

    @Test
    public void testarCriarInvestimento(){

        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);

        Assertions.assertEquals(investimento,investimentoObjeto);

    }

    @Test
    public void testarBuscarInvestimentoPorId(){

        Optional<Investimento> optionalInvestimento = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(optionalInvestimento);

        Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimentoPorId(investimento.getIdInvest());

        Assertions.assertEquals(investimento,investimentoOptional.get());

    }

    @Test
    public void testarBuscarInvestimentoPorIdInexistente(){

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenThrow(new ObjectNotFoundException(Investimento.class, "Investimento Não Encontrado"));

        Assertions.assertThrows(ObjectNotFoundException.class, () -> { investimentoService.buscarInvestimentoPorId(investimento.getIdInvest());});

    }

    @Test
    public void testarAtualizarInvestimento(){

        Optional<Investimento> optionalInvestimento = Optional.of(investimento);

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(optionalInvestimento);

        investimento.setNomeInvest("Teste Atualizar Investimento");

        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoAtualizado = investimentoService.atualizarInvestimento(investimento);

        Assertions.assertEquals(investimento.getNomeInvest(),investimentoAtualizado.getNomeInvest());
    }

    @Test
    public void testarAtualizarInvestimentoInexistente(){

        Optional<Investimento> optionalInvestimento = Optional.empty();

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(optionalInvestimento);

        investimento.setNomeInvest("Teste Atualizar Investimento");

        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> { investimentoService.atualizarInvestimento(investimento);});
    }

    @Test
    public void testarDeletar(){
        investimentoService.apagarInvestimento(investimento);
        Mockito.verify(investimentoRepository, Mockito.times(1)).delete(Mockito.any(Investimento.class));
    }

    @Test
    public void testarSimulacao(){
        simulacao = new Simulacao();
        simulacao.setDinheiroAplicado(1000);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(10);

        Assertions.assertEquals(investimentoService.calcularSimulacao(simulacao, investimento.getPercentInvest()), 2100.0);
    }


}
