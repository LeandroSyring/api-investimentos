package com.investimentos.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.ResultadoSimulacao;
import com.investimentos.models.Simulacao;
import com.investimentos.security.JWTUtil;
import com.investimentos.services.InvestimentoService;
import com.investimentos.services.UsuarioService;
import org.hamcrest.CoreMatchers;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
@Import(JWTUtil.class)
public class InvestimentoControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    InvestimentoService investimentoService;

    ObjectMapper mapper = new ObjectMapper();

    Investimento investimento;

    Simulacao simulacao;

    ResultadoSimulacao resultadoSimulacao;

    @BeforeEach
    public void iniciar(){
        investimento = new Investimento();
        investimento.setIdInvest(1);
        investimento.setDscInvest("Descricao Investimento");
        investimento.setNomeInvest("Nome Investimento");
        investimento.setPercentInvest(10);
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.ALTO);
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarCriarInvestimento() throws Exception {

        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeInvest", CoreMatchers.equalTo(investimento.getNomeInvest())));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarInvestimentoPorId() throws Exception {

        Optional optionalInvestimento = Optional.of(investimento);

        Mockito.when(investimentoService.buscarInvestimentoPorId(Mockito.anyInt())).thenReturn(optionalInvestimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeInvest",CoreMatchers.equalTo(investimento.getNomeInvest())))
                .andExpect((MockMvcResultMatchers.status().isOk()));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarInvestimentoPorIdSemResultado() throws Exception {

        Optional optionalInvestimento = Optional.empty();

        Mockito.when(investimentoService.buscarInvestimentoPorId(Mockito.anyInt())).thenReturn(optionalInvestimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1"))
                .andExpect((MockMvcResultMatchers.status().isBadRequest()));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarAtualizarInvestimento() throws Exception {

        investimento.setNomeInvest("Teste Atualizar Investimento");

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeInvest", CoreMatchers.equalTo("Teste Atualizar Investimento")));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarAtualizarInvestimentoInexistente() throws Exception {

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenThrow(new ObjectNotFoundException(Investimento.class, "Investimento Não Encontrado"));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarApagar() throws Exception {

        Optional optionalInvestimento = Optional.of(investimento);

        Mockito.when(investimentoService.buscarInvestimentoPorId(Mockito.anyInt())).thenReturn(optionalInvestimento);

        investimentoService.apagarInvestimento(investimento);
        Mockito.verify(investimentoService).apagarInvestimento(Mockito.any(Investimento.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarApagarInexistente() throws Exception {

        Mockito.when(investimentoService.buscarInvestimentoPorId(Mockito.anyInt())).thenThrow(new ObjectNotFoundException(Investimento.class, "Investimento Não Encontrado"));

        investimentoService.apagarInvestimento(investimento);
        Mockito.verify(investimentoService).apagarInvestimento(Mockito.any(Investimento.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarGerarSimulacao() throws Exception{

        Optional optionalInvestimento = Optional.of(investimento);

        Mockito.when(investimentoService.buscarInvestimentoPorId(Mockito.anyInt())).thenReturn(optionalInvestimento);

        simulacao = new Simulacao();
        simulacao.setDinheiroAplicado(1000);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(10);

        resultadoSimulacao = new ResultadoSimulacao();
        resultadoSimulacao.setResultadoSimulacao(2100.0);

        Mockito.when(investimentoService.calcularSimulacao(Mockito.any(Simulacao.class), Mockito.anyDouble())).thenReturn(resultadoSimulacao.getResultadoSimulacao());

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulacao", CoreMatchers.equalTo(resultadoSimulacao.getResultadoSimulacao())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarGerarSimulacaoInvestimentoInexistente() throws Exception{

        Mockito.when(investimentoService.buscarInvestimentoPorId(Mockito.anyInt())).thenThrow(new ObjectNotFoundException(Investimento.class, "Investimento Não Encontrado"));

        simulacao = new Simulacao();
        simulacao.setDinheiroAplicado(1000);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(10);

        resultadoSimulacao = new ResultadoSimulacao();
        resultadoSimulacao.setResultadoSimulacao(2100.0);

        Mockito.when(investimentoService.calcularSimulacao(Mockito.any(Simulacao.class), Mockito.anyDouble())).thenReturn(resultadoSimulacao.getResultadoSimulacao());

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
