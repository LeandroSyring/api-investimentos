package com.investimentos.enums;

public enum RiscoDoInvestimento {
    ALTO,
    BAIXO,
    MEDIO
}
