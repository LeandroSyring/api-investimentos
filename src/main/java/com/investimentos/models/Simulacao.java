package com.investimentos.models;

import javax.validation.constraints.DecimalMin;

public class Simulacao {


    private Integer investimentoId;

    private Integer mesesDeAplicacao;

    @DecimalMin("100.00")
    private double dinheiroAplicado;

    public Simulacao(){

    }

    public Simulacao(Integer id, Integer qtdMeses, double vlrInvestimento) {
        this.investimentoId = id;
        this.mesesDeAplicacao = qtdMeses;
        this.dinheiroAplicado = vlrInvestimento;
    }

    public Integer getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(Integer investimentoId) {
        this.investimentoId = investimentoId;
    }

    public Integer getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(Integer mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
