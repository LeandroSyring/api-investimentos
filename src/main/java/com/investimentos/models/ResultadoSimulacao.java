package com.investimentos.models;

public class ResultadoSimulacao {

    private double resultadoSimulacao;

    public ResultadoSimulacao(){

    }

    public ResultadoSimulacao(double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }

    public double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }
}
