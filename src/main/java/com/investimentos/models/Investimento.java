package com.investimentos.models;

import com.investimentos.enums.RiscoDoInvestimento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idInvest;


    @NotBlank
    @NotNull
    private String nomeInvest;

    @NotNull
    private String dscInvest;

    @NotNull
    @DecimalMin("0.1")
    private double percentInvest;

    @NotNull
    private RiscoDoInvestimento riscoDoInvestimento;

    public Investimento(){

    }

    public Investimento(Integer idInvest, String nomeInvest, String dsc_Invest, double percent_Invest, RiscoDoInvestimento riscoDoInvestimento) {
        this.idInvest = idInvest;
        this.nomeInvest = nomeInvest;
        this.dscInvest = dsc_Invest;
        this.percentInvest = percent_Invest;
        this.riscoDoInvestimento = riscoDoInvestimento;
    }

    public Integer getIdInvest() {
        return idInvest;
    }

    public void setIdInvest(Integer idInvest) {
        this.idInvest = idInvest;
    }

    public String getNomeInvest() {
        return nomeInvest;
    }

    public void setNomeInvest(String nomeInvest) {
        this.nomeInvest = nomeInvest;
    }

    public String getDscInvest() {
        return dscInvest;
    }

    public void setDscInvest(String dscInvest) {
        this.dscInvest = dscInvest;
    }

    public double getPercentInvest() {
        return percentInvest;
    }

    public void setPercentInvest(double percentInvest) {
        this.percentInvest = percentInvest;
    }

    public RiscoDoInvestimento getRiscoDoInvestimento() {
        return riscoDoInvestimento;
    }

    public void setRiscoDoInvestimento(RiscoDoInvestimento riscoDoInvestimento) {
        this.riscoDoInvestimento = riscoDoInvestimento;
    }
}
