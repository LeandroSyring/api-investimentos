package com.investimentos.controllers;


import com.investimentos.models.Investimento;
import com.investimentos.models.ResultadoSimulacao;
import com.investimentos.models.Simulacao;
import com.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    public ResponseEntity<Investimento> criarInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento investimentoObject = investimentoService.criarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObject);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Investimento> buscarInvestimentoPorId(@PathVariable Integer id){
        try {
            Investimento investimento = investimentoService.buscarInvestimentoPorId(id).get();
            return ResponseEntity.status(200).body(investimento);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Investimento>  atualizarInvestimento(@PathVariable Integer id, @RequestBody @Valid Investimento investimento){
        investimento.setIdInvest(id);
        try {
            Investimento investimentoObject = investimentoService.atualizarInvestimento(investimento);
            return ResponseEntity.status(200).body(investimentoObject);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Investimento> apagarInvestimento(@PathVariable Integer id){

        try {
            Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimentoPorId(id);
            investimentoService.apagarInvestimento(investimentoOptional.get());
            return ResponseEntity.status(204).body(null);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @PutMapping("/simulacao")
    public ResponseEntity<ResultadoSimulacao> gerarSimulacao(@RequestBody @Valid Simulacao simulacao){

       try {

           Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimentoPorId(simulacao.getInvestimentoId());

           ResultadoSimulacao resultadoSimulacao = new ResultadoSimulacao(investimentoService.calcularSimulacao(simulacao, investimentoOptional.get().getPercentInvest()));

           return ResponseEntity.status(200).body(resultadoSimulacao);
       }
       catch (Exception e){
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
       }
    }


}