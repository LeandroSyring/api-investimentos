package com.investimentos.services;

import com.investimentos.models.Investimento;
import com.investimentos.models.Simulacao;
import com.investimentos.repositories.InvestimentoRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento criarInvestimento(Investimento investimento){
        Investimento objectInvestimento = investimentoRepository.save(investimento);
        return objectInvestimento;
    }

    public Optional<Investimento> buscarInvestimentoPorId(int id){
        Optional<Investimento> investimentoOption = investimentoRepository.findById(id);

        if(investimentoOption.isPresent()){
            return investimentoOption;
        }
        else{
            throw new ObjectNotFoundException(Investimento.class, "Investimento Não Encontrado");
        }
    }

    public Investimento atualizarInvestimento(Investimento investimento){

        try {
            Optional<Investimento> investimentoOptional = buscarInvestimentoPorId(investimento.getIdInvest());
            Investimento objectInvestimento = investimentoRepository.save(investimento);
            return objectInvestimento;
        }
        catch (Exception e){
            throw new ObjectNotFoundException(Investimento.class, e.getMessage());
        }
    }

    public void apagarInvestimento(Investimento investimento){
        investimentoRepository.delete(investimento);
    }

    public double calcularSimulacao(Simulacao simulacao, double percentInvest){

        double resultadoDaSimulacao = 0.00;
        for (int i = 0; i <= simulacao.getMesesDeAplicacao(); i++){
            resultadoDaSimulacao += percentInvest * simulacao.getDinheiroAplicado() / 100;
        }
        resultadoDaSimulacao += simulacao.getDinheiroAplicado();

        return resultadoDaSimulacao;
    }

}
